/*
 * parse.h globals for parse.c
 */
#ifndef POL_SYMLIST
#define POL_SYMLIST
#define ASCII_NUM_LOW_B 060	/* Ascii code for 0, lowest digit bound */
#define ASCII_NUM_TOP_B 071	/* Ditto, but max */
#define MAX_OPERATORS 2		/* Maximum of operands an operator can have */
#include "input.h"
#include "stdlib.h"

typedef enum {
	NO_MATCH, MATCHES, MATCHES_NEEDS_CLOSING
} RESULT;

/* Struct returned by cmp_to_symlist */
typedef struct {
	RESULT		r;
	char		req_closing_symbol;
} cmp_result;

/*
 * Holds a basic operator, like /+-* or a function, and the allowed operands
 * (X+Y=2 operands, log(X)=1 operand, sum(4,5,4) = 0 operands
 */
typedef struct {
	char		name;	/* TODO: accept long names like log() */
	char		closing_symbol;	/* 0 if none */
	int		allowed_operands;	/* 0 if not defined, like
						 * parenthesis */
} operator;

extern operator syms[];

/*
 * Mode this program is running in: expression parsing (4+1*(7-3)) or reverse
 * polish notation
 */
typedef enum {
	EXP, RPN
} PMODE;

/*
 * Holds an operator and operator.allowed_operands operands (also named
 * semantic_units)
 */
typedef struct semantic_compound {
	char		op;
	union {
		int		unit;
		struct semantic_compound *await_result;
	}		operand_1;	/* Holds the first operand, could be
					 * a literal (like a number) or
					 * another compound's result */
	union {
		int		unit;
		struct semantic_compound *await_result;
	}		operand_2;
}		semantic_compound;

typedef struct {
	char		symbol[BUF_MAX_SIZE];	/* List of parsed
						 * non-numerical symbols */
	char		match[BUF_MAX_SIZE];	/* List of closing symbols
						 * for the list above, NULL
						 * if no match is required */
	int		pos[BUF_MAX_SIZE];	/* List of indices for symbol
						 * list */
	int		posclose[BUF_MAX_SIZE];	/* List of indeces for
						 * closing symbol list */
	char	       *semantic_compound[BUF_MAX_SIZE];	/* list of semantic
								 * compounds, which are
								 * formed by 1 (one)
								 * operator, and
								 * operator.allowed_operands
								 * operands */
	int		last;	/* Index of last character in both symbol and
				 * match lists */
} lextree;

typedef struct {
	char	       *current;	/* Current symbol on the stack */
	int		needs_match;	/* Signals if current symbol expects
					 * a matching one */
	char		expected_match;	/* If above is true, holds expected
					 * symbol */
	PMODE		mode;
	semantic_compound *compounds;
	lextree		tree;
} parse_status;


void		argv_handle(parse_status * ps, int argc, char *argv[]);
void		lex(char *uinput, parse_status * ps);
void		parse(char *uinput, parse_status * ps);
cmp_result	cmp_to_symlist(char c);
#endif				/* POL_SYMLIST */
