/*
 * input.h header definitions for user input
 */

#ifndef PARSE_GLOBALS
#define PARSE_GLOBALS
#define BUF_MAX_SIZE 512	/* Maximum size allocated for user input */
#define PROMPT_ASCII ">>\n"

void		prompt();

#endif
