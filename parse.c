/*
 * parse.c parse mathematical expressions
 */

#include "parse.h"
#include <string.h>
#include <stdio.h>
#include <err.h>

operator	syms[] = {
	{'(', ')', 0},
	{'[', ']', 0},
	{'{', '}', 0},
	{'+', 0, 2},
	{'-', 0, 2},
	{'^', 0, 2},
	{'/', 0, 2},
	{'*', 0, 2},
	{'%', 0, 2},
{0}};

void
argv_handle(parse_status * ps, int argc, char *argv[])
{
	for (int arg = 0; arg < argc; arg++) {
		if (strcmp(argv[arg], "-pn"))
			ps->mode = RPN;
		else
			ps->mode = EXP;
	}
	return;
}

cmp_result
cmp_to_symlist(char c){
	cmp_result	res = {NO_MATCH, 0};
	for (int sym = 0; syms[sym].name != 0; sym++) {	/* Check for every
							 * possible operator
							 * until we find the
							 * NULL end */
		if (c == syms[sym].name) {	/* Check if operator requires
						 * a closing match */
			if (syms[sym].closing_symbol != 0) {
				printf("%c: requires closing\n", c);
				res.r = MATCHES_NEEDS_CLOSING;
				res.req_closing_symbol = syms[sym].closing_symbol;
			} else
				res.r = MATCHES;
		}
	}
	return res;
}

/* lex: creates a lexical tree with operator symbols */
void
lex(char *uinput, parse_status * ps)
{
	for (int c = 0; uinput[c] != 0; c++) {	/* Iterate string until '\0' */
		/*
		 * Quick check to see if we are reading a number or other
		 * thing (like an operator inside syms). This saves
		 * unnecessary loops.
		 */
		if (uinput[c] > ASCII_NUM_LOW_B && uinput[c] < ASCII_NUM_TOP_B) {	/* We found a number */
			continue;
		} else if (ps->needs_match == 0) {	/* Found a symbol */
			cmp_result	res = cmp_to_symlist(uinput[c]);
			ps->needs_match = (res.r == MATCHES_NEEDS_CLOSING);
			ps->expected_match = res.req_closing_symbol;
			ps->tree.symbol[ps->tree.last] = uinput[c];	/* Copy the symbol to
									 * the symbol stack */
			ps->tree.pos[ps->tree.last] = c;
			if (res.r == MATCHES)
				ps->tree.last++;
		} else if (ps->needs_match == 1 && ps->expected_match != uinput[c]) {	/* Found symbol, but not
											 * the one we are
											 * looking for */
			/* TODO: nested parenthesis (leave for the end) */
			fputs("nested operations are not supported yet\n", stdout);
			exit(1);

		} else if (ps->needs_match == 1 && ps->expected_match == uinput[c]) {	/* Found the matching
											 * symbol we were
											 * looking for */
			ps->needs_match = 0;	/* Reset looking for flag
						 * match */
			ps->expected_match = 0;	/* Reset looking for symbol */
			ps->tree.match[ps->tree.last] = uinput[c];	/* Add symbol to the
									 * matching symbols
									 * list, in the exact
									 * same index as the
									 * opening */
			ps->tree.posclose[ps->tree.last] = c;
			ps->tree.last++;	/* Increase last symbol index
						 * for next iteration */
		}
	}
	parse(uinput, ps);
}
/* parse: completes the lexical tree after reading symbols */
void
parse(char *uinput, parse_status * ps)
{
	int		position_index = 0, next_operator = 0;
	int		literal_index = 0;
	int		compounds_index = 0;
	int		old_next_operator = 0;
	ps->compounds = malloc(sizeof(semantic_compound) * ps->tree.last);	/* Allocate operators+1
										 * semantic compounds */
	if (ps->compounds == NULL)
		err(1, "could not allocate memory for semantic compounds");
	next_operator = ps->tree.pos[position_index];	/* Mark next operator
							 * found on the previous
							 * lex step */
	char	       *literal = malloc(sizeof(char) * next_operator);	/* Allocate enough
									 * characters to store a
									 * number literal, later
									 * to be processed by
									 * sscanf */
	if (literal == NULL)
		err(1, "could not allocate memory to parse literals");
	/*
	 * Start iterating input, adding each literal before & after an
	 * operator to the semantic_compound operands
	 */

	for (int uinput_index = 0; uinput[uinput_index] != 0; uinput_index++) {
		if (uinput_index == next_operator && position_index != 0) {
			next_operator = ps->tree.pos[++position_index];
			literal = 0;
		} else if (uinput_index == next_operator) {
			ps->compounds[compounds_index].op = ps->tree.symbol[position_index];
			sscanf(literal, "%d", &ps->compounds[compounds_index].operand_1.unit);
			literal = reallocarray(literal, next_operator - old_next_operator, sizeof(char));
			old_next_operator = next_operator;
			next_operator = ps->tree.pos[++position_index];
		}
		literal[literal_index++] = uinput[uinput_index];
	}
}
