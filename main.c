/*
 * polparse a simple math expression parser with support for reverse polish
 * notation
 */
#include "input.h"
#include "parse.h"
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int
main(int argc, char *argv[])
{
	/*
	 * TODO: which input buffer to operate in, stdin unless specified
	 * otherwise
	 */
	FILE	       *default_buffer = stdin;
	parse_status	ps;
	ps.expected_match = 0;
	ps.current = NULL;
	ps.needs_match = 0;
	ps.mode = EXP;		/* Expression parsing by default */
	lextree		tree = {{0}, {0}, {0}, {0}, 0};
	ps.tree = tree;
//TODO:	initialize ps before passing it to lex
		argv_handle(&ps, argc, argv);
	/* User input handling */
	char	       *uinput;
	/* Allocate BUF_MAX_SIZE characters for user input */
	if ((uinput = malloc(sizeof(char) * BUF_MAX_SIZE)) == NULL) {
		err(1, "malloc: could not alloc user input");
	}
	/* Prompt and read user input */
	printf("Please input an expression, or send SIGINFO (Ctrl+T) to change to RPN mode\n");
//TODO:	actually handle signals
		prompt(ps.mode);
	fgets(uinput, BUF_MAX_SIZE, default_buffer);
	lex(uinput, &ps);
	fputs(uinput, stdout);
	free(uinput);
}
