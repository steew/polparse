/*
 * input.c this file handles user input (but does not parse anything)
 */
#include "input.h"
#include <stdio.h>

void
prompt(int mode)
{
	if (mode != -1)
		fprintf(stdout, "%d%s", mode, PROMPT_ASCII);	/* Mode has been
								 * changed, reflect it */
	else
		fprintf(stdout, "%s", PROMPT_ASCII);
}
