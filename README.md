# polparse

simple math expression parser and RPN calculator

## build instructions
Build as any other meson project.
```sh
meson build && cd build && ninja
```

